use std::env;
use std::fs;
use std::io::prelude::*;
use std::error::Error;


struct Config {
    query: String,
    filename: String,
}

impl Config {
    fn new(args: &[String]) -> Result<Config, &'static str>{
        if args.len() < 3 {
            return Err("not enough arguments");
        }
        let query = args[1].clone();
        let filename = args[2].clone();
        Ok(Config{query, filename})
    }

}


fn run(config: Config) -> Result<(), Box<dyn Error>>{
    let content = fs::read_to_string(config.filename)
        .expect("Something went wrong reading the file\n");

    println!("With text: \n {}", content);      
    Ok(())
}


fn main() {
    let args: Vec<String> = env::args().collect();

    let config = Config::new(&args).unwrap_or_else(|err|{
        println!("Problem parsing arguments: {}", err);
        process::exit();
    });

    println!("Searching for {} \nIn file {}", config.query, config.filename);

    run(config);

}


